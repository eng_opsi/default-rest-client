package it.eng.digitalenabler.restclient;

import java.net.URI;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import it.eng.digitalenabler.restclient.HTTPResponse;
import it.eng.digitalenabler.restclient.RestClient;
import it.eng.digitalenabler.restclient.exception.ForbiddenException;
import it.eng.digitalenabler.restclient.exception.UnauthorizedException;

public class DefaultRestClient implements RestClient {

	public HTTPResponse consumePost(String url, Object body, Map<String, String> headers)
			throws ClientHandlerException, UniformInterfaceException, Exception {
		Client client = Client.create();
		String mediaType = headers.get("Content-Type");
		headers.remove("Content-Type");
		Builder builder = client.resource(url).type(mediaType!=null ? mediaType : MediaType.APPLICATION_JSON);

		if (headers != null) {
			Set<Map.Entry<String, String>> hs = headers.entrySet();
			for (Map.Entry<String, String> h : hs) {
				builder = builder.header(h.getKey(), h.getValue());
			}
		}

		ClientResponse resp = builder.post(ClientResponse.class, body.toString());

		if (resp.getStatus() == 401) {
			throw new UnauthorizedException("URL " + url + " responded with status " + resp.getStatus());
		}

		if (resp.getStatus() == 403) {
			throw new ForbiddenException("URL " + url + " responded with status " + resp.getStatus() + " "
					+ resp.getStatusInfo().getReasonPhrase());
		}

		if (resp.getStatus() > 301) {
			throw new Exception("URL " + url + " responded with status " + resp.getStatus() + " and message: "
					+ resp.getEntity(String.class));
		}

		String r = "";
		if (resp.getStatus() != HttpStatus.SC_NO_CONTENT && resp.hasEntity()) {
			r = resp.getEntity(String.class);
		} else {
			try {
				resp.close();
			} catch (Exception e) {
				System.err.println("Cannot close connection");
			}
		}

		HTTPResponse out = new HTTPResponse();
		out.setResponseCode(resp.getStatus());
		out.setBody(r);
		out.setHeaders(resp.getHeaders());

		return out;
	}

	public HTTPResponse consumeGet(String url, Map<String, String> headers)
			throws ClientHandlerException, UniformInterfaceException, Exception {
		Client client = Client.create();
		Builder builder = client.resource(url).getRequestBuilder();

		if (headers != null) {
			Set<Map.Entry<String, String>> hs = headers.entrySet();
			for (Map.Entry<String, String> h : hs) {
				builder = builder.header(h.getKey(), h.getValue());
			}
		}

		ClientResponse resp = builder.get(ClientResponse.class);

		if (resp.getStatus() > 301) {
			throw new Exception("URL " + url + " responded with status " + resp.getStatus() + " and message: "
					+ resp.getEntity(String.class));
		}

		String r = "";
		if (resp.getStatus() != HttpStatus.SC_NO_CONTENT && resp.hasEntity()) {
			r = resp.getEntity(String.class);
		} else {
			try {
				resp.close();
			} catch (Exception e) {
				System.err.println("Cannot close connection");
			}
		}

		HTTPResponse out = new HTTPResponse();
		out.setResponseCode(resp.getStatus());
		out.setBody(r);
		out.setHeaders(resp.getHeaders());

		return out;
	}

	public HTTPResponse consumeDelete(String url, Map<String, String> headers)
			throws ClientHandlerException, UniformInterfaceException, Exception {
		Client client = Client.create();
		Builder builder = client.resource(url).getRequestBuilder();
		if (headers != null) {
			Set<Map.Entry<String, String>> hs = headers.entrySet();
			for (Map.Entry<String, String> h : hs) {
				builder = builder.header(h.getKey(), h.getValue());
			}
		}
		ClientResponse resp = builder.delete(ClientResponse.class);
		String r = "";
		if (resp.getStatus() > 301) {
			throw new Exception("URL " + url + " responded with status " + resp.getStatus() + " and message: "
					+ resp.getEntity(String.class));
		} else {
			if (resp.hasEntity())
				r = resp.getEntity(String.class);
			try {
				resp.close();
			} catch (Exception e) {
				System.err.println("Cannot close connection");
			}
		}

		HTTPResponse out = new HTTPResponse();
		out.setResponseCode(resp.getStatus());
		out.setBody(r);
		out.setHeaders(resp.getHeaders());

		return out;
	}

	public HTTPResponse consumePut(String url, Object body, String type, Map<String, String> headers)
			throws ClientHandlerException, UniformInterfaceException, Exception {
		Client client = Client.create();
		Builder builder = client.resource(url).type(type);

		if (headers != null) {
			Set<Map.Entry<String, String>> hs = headers.entrySet();
			for (Map.Entry<String, String> h : hs) {
				builder = builder.header(h.getKey(), h.getValue());
			}
		}

		ClientResponse resp = builder.put(ClientResponse.class, body.toString());
		String r = "";
		if (resp.getStatus() > 301) {
			throw new Exception("URL " + url + " responded with status " + resp.getStatus() + " and message: "
					+ resp.getEntity(String.class));
		} else {
			if (resp.hasEntity())
				r = resp.getEntity(String.class);
			try {
				resp.close();
			} catch (Exception e) {
				System.err.println("Cannot close connection");
			}
		}

		HTTPResponse out = new HTTPResponse();
		out.setResponseCode(resp.getStatus());
		out.setBody(r);
		out.setHeaders(resp.getHeaders());

		return out;
	}

	public HTTPResponse consumePatch(String url, Object body, Map<String, String> headers) throws Exception {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HTTPResponse out = new HTTPResponse();
		
		try {
			HttpPatch httpPatch = new HttpPatch(new URI(url));

			StringEntity entity = new StringEntity(body.toString());
			entity.setContentType("application/json");
			httpPatch.setEntity(entity);

			if (headers != null) {
				Set<Map.Entry<String, String>> hs = headers.entrySet();
				for (Map.Entry<String, String> h : hs) {
					httpPatch.setHeader(h.getKey(), h.getValue());
				}
			}

			CloseableHttpResponse response = httpClient.execute(httpPatch);
			try {
				int status = response.getStatusLine().getStatusCode();
				MultivaluedMap<String, String> mMap = new MultivaluedMapImpl();
				for(Header header: response.getAllHeaders()) {
					mMap.add(header.getName(),header.getValue());
				}
				out.setResponseCode(status);
				out.setBody(response.getEntity().toString());
				out.setHeaders(mMap);
				
				if (status > 301) {
					throw new Exception("URL " + url + " responded with status " + status);
				}
			} finally {
				response.close();
			}
		} finally {
			httpClient.close();
		}

		return out;
	}

}
